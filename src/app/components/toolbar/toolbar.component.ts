import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  user = null;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
      this.userService.user.subscribe(data => {
        this.user = data
      })
        if(localStorage.getItem("user")) {
          this.userService.user.next(JSON.stringify(localStorage.getItem("user")))
        }
  }

  public logout(){
    this.userService.user.next("")
    this.router.navigate(["/"])
    localStorage.clear()
    this.user = null
    return () => {
      this.userService.user.unsubscribe()
    }
  }

}
