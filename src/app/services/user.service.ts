import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Subject} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  user = new Subject();

  public addUser(obj){
    return this.http.post("http://localhost:3000/users", obj)
  }

  public getUser(email){
    return this.http.get("http://localhost:3000/users?email=" + email)
  }

}
