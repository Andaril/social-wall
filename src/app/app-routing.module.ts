import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./pages/login/login.component";
import {CreateAccountComponent} from "./pages/create-account/create-account.component";
import {PostsComponent} from "./pages/posts/posts.component";

const routes: Routes = [
  {component: LoginComponent, path: "login"},
  {component: CreateAccountComponent, path: "create-account"},
  {redirectTo: "/login", path: "", pathMatch: "full"},
  {component: PostsComponent, path: "posts"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
