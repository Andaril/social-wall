import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  constructor(private fb: FormBuilder, private userServer: UserService, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
  }

  createLoginForm: FormGroup = this.fb.group({
    email: ["", [Validators.required, Validators.email]],
    userName: ["", [Validators.required, Validators.minLength(6)]],
    password: ["", [Validators.required, Validators.minLength(10)]]
  })

  public addUser(user){
    const subscription = this.userServer.addUser(user).subscribe({
      next: user => {
        this.snackBar.open("Success")
        this.router.navigate(["login"])
      },
      error: err => this.snackBar.open("Error: " + err)
    })
    return () => {
      subscription.unsubscribe()
    }
  }

}
