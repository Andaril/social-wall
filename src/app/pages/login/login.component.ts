import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private userService: UserService, private snack: MatSnackBar, private router: Router) {}

  ngOnInit(): void {}

  loginForm: FormGroup = this.fb.group({
    email: ["", [Validators.required, Validators.email]],
    password: ["", [Validators.required, Validators.minLength(10)]]
  })

  public authUser(email){
    const subscription = this.userService.getUser(email).subscribe({
      next: dataUser => {
        if(dataUser) {
          if(dataUser[0] && (dataUser[0].password === this.loginForm.value.password)){
            this.snack.open("Success")
            this.userService.user.next(dataUser[0])
            this.router.navigate(["posts"])
            localStorage.setItem("user", dataUser[0])
          }
        }
      },
      error: err => {
        console.log(err, "Error authentication")
        this.snack.open("Invalid email or password")
      },
      complete: () => console.log("Finish")
    })

    return () => {
      subscription.unsubscribe()
    }
  }

}
